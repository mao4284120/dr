js:
var 一级定位模板 = [];
var 二级定位模板 = [];
var 搜索定位模板 = [];
var 首页定位模板 = [];
var root = 'hiker://files/rules/dzHouse/json/'; //模板根目录
function mubans_init() { //初始化模板
    let api = 'https://gitlab.com/mao4284120/dr/-/raw/master/json/';
    let mubans = {
        首页模板: api + '首页模板.json',
        一级模板: api + '一级模板.json',
        二级模板: api + '二级模板.json',
        搜索模板: api + '搜索模板.json',
    };
    requireDownload(mubans.首页模板, `${root}首页模板.json`);
    requireDownload(mubans.一级模板, `${root}一级模板.json`);
    requireDownload(mubans.二级模板, `${root}二级模板.json`);
    requireDownload(mubans.搜索模板, `${root}搜索模板.json`);
}

mubans_init();
require(config.模板); //超级牛逼，只要使用自动匹配模板初始化就自动依赖dr模板

function mubans(inner, local) { //模板合并 代码内置模板与本地文件模板
    // local: 一级模板,二级模板,搜索模板
    // writeFile(root+local+'.json',JSON.stringify(inner));
    // return inner

    let code = fetch(root + local + '.json');
    try {
        let localYj = JSON.parse(code);
        // let localYj = eval(fetch(root+local+'.json'));
        let newMb = [];
        if (Array.isArray(localYj) && localYj.length > 0) {
            for (let it of localYj) {
                let idex = inner.findIndex(x => x.名称 === it.名称);
                if (idex > -1) {
                    inner[idex] = it
                } else {
                    newMb.push(it)
                }
            }
            inner = inner.concat(newMb)
        }
        return inner
    } catch (e) {
        log(`本地${local}:${root + local + '.json'}有误:` + e.message);
        log(code);
        return inner
    }
}
首页定位模板.push({
    "名称": "hl-vod-list",
    "标题": "h2,0&&Text$$h2,1&&Text$$h2,2&&Text$$h2,3&&Text$$h2,4&&Text$$h2,5&&Text$$h2,6&&Text$$h2,7&&Text",
    "海报": "",
    "解析": ".hl-vod-list,0&&.hl-list-item;a&&title||Text;img||a&&data-original||data-src||data-echo||data-background||src||href;.hl-pic-text&&Text;a&&href$$.hl-rank-list,0&&li;a&&title||Text;.hl-lazy&&data-original||src||data-src||data-echo;.hl-item-data&&Text;a&&href$$.hl-vod-list,1&&.hl-list-item;a&&title||Text;img||a&&data-original||data-src||data-echo||src||href;.hl-pic-text&&Text;a&&href$$.hl-rank-list,1&&li;a&&title||Text;.hl-lazy&&data-original||src||data-src||data-echo;.hl-item-data&&Text;a&&href$$.hl-vod-list,2&&.hl-list-item;a&&title||Text;img||a&&data-original||data-src||data-echo||src||href;.hl-pic-text&&Text;a&&href$$.hl-rank-list,2&&li;a&&title||Text;.hl-lazy&&data-original||src||data-src||data-echo;.hl-item-data&&Text;a&&href$$.hl-vod-list,3&&.hl-list-item;a&&title||Text;img||a&&data-original||data-src||data-echo||src||href;.hl-pic-text&&Text;a&&href$$.hl-rank-list,3&&li;a&&title||Text;.hl-lazy&&data-original||src||data-src||data-echo;.hl-item-data&&Text;a&&href",
    "一级处理": {},
    "关闭折叠": false,
    "设置倒放": false,
    "是否有二级": true,
    "周报": "",
    "免责": false
});
首页定位模板 = mubans(首页定位模板, '首页模板');
一级定位模板.push({
    名称: '阿房影视',
    自动初始分类: true,
    初始分类链接定位: '.myui-header__menu&&li,1&&a&&href',
    初始分类链接处理: ((u) => {
        return u.replace(/\/type\/(.*?)\//, '/show/$1--------1---');
    }).toString(),
    解析: '.myui-vodlist&&li;.lazyload&&title;.lazyload&&data-original;.lazyload&&Text;.lazyload&&href',
    动态分类列表: [{
            一级分类: 'body&&ul.myui-header__menu',
            子分类: 'body&&li.hidden-sm:gt(0):lt(5)',
            分类链接: "@js:(()=>{url=pd(input,'a&&href');return url.replace(/vod(\\/?)\\w+/,'vod$1show')})()"
        }, {
            一级分类: 'body&&.myui-screen__list',
            子分类: 'ul&&li:has(a[href]):not(:matches(^$))',
        }
    ],
    一级处理: {},
    关闭折叠: false,
    设置倒放: false,
    是否有二级: true,
    免责: false,
});
一级定位模板 = mubans(一级定位模板, '一级模板');
二级定位模板 = [{
        解析: {
            title: '.title&&Text;.data,3&&Text;.data,7&&Text',
            img: '.lazyload&&data-original',
            url: '.lazyload&&data-originalurl&&a&&href',
            desc: '.data&&Text;.data,6&&Text',
            content: 'p.detail&&Text.js:input.replace("简介：","").replace("详情","")',
            tabs: '.nav-tabs&&li',
            lists: 'body&&.stui-content__playlist,#id&&li',
            tab_id: '',
        },
        名称: '蓝莓影视',
        免嗅: false,
        需要魔断: false,
        动态最新章节: true,
        二级处理: {},
    }, {
        解析: {
            title: 'h1.title&&Text;p.data,1&&Text',
            img: '.lazyload&&data-original',
            desc: 'span.sketch.content&&Text',
            tabs: '.nav-tabs&&li',
            lists: '.tab-content&&div,#id&&ul&&li',
        },
        名称: '极品影视',
        免嗅: false,
        需要魔断: false,
        动态最新章节: true,
    }, {
        解析: {
            title: 'h1&&Text;.module-info-tag&&Text',
            img: '.lazyload&&data-original',
            desc: '.module-info-introduction-content&&Text',
            tabs: 'body&&.module-tab-item',
            lists: 'body&&.module-play-list,#id&&a',
        },
        名称: '极客影视',
        免嗅: false,
        需要魔断: false,
        动态最新章节: true,
    }, {
        解析: {
            title: 'h1.title&&Text;.data,1&&Text;#rating&&.branch&&Text',
            img: '.myui-content__thumb&&a&&img&&data-original',
            url: '.myui-content__thumb&&a&&href',
            desc: '.data,2&&Text;.data,3&&Text',
            content: '#desc&&span.data&&Text',
            tabs: 'ul.nav-tabs&&li',
            lists: '.tab-content&&#id&&li',
            tab_id: 'a&&href'
        },
        名称: '阿房影视',
        免嗅: false,
        需要魔断: false,
        动态最新章节: true,
    }
];
二级定位模板 = mubans(二级定位模板, '二级模板');
搜索定位模板 = [{
        解析: '.stui-vodlist&&li;a&&title;.stui-vodlist__thumb&&data-original;.pic-text&&Text;a&&href;.stui-vodlist__item&&Text',
        名称: '蓝莓影视',
        一级处理: {},
        是否有二级: true,
        免责: false,
    }, {
        解析: '.myui-vodlist__media#searchList&&li;a&&title;a&&data-original;.pic_text&&Text;a&&href;p,3&&Text',
        名称: '极品影视',
        是否有二级: true,
        免责: false,
    }, {
        解析: 'body&&.module-item;.module-card-item-title&&Text;.lazyload&&data-original;.module-item-note&&Text;a&&href;.module-info-item-content&&Text',
        名称: '极客影视',
        是否有二级: true,
        免责: false,
    }
];
搜索定位模板 = mubans(搜索定位模板, '搜索模板');
var 自动一级分类 = function () {
    依赖检测(); //一级界面检测依赖
    MY_URL = MY_URL.replace("hiker://empty##", "").split('#')[0];
    var 规则处理 = typeof(一级处理) !== 'undefined';
    var page = MY_PAGE;
    var 匹配成功 = false;
    for (let i in 一级定位模板) {
        let it = 一级定位模板[i];
        let parStr = it.解析;
        if (!规则处理) { //规则没写处理
            if (it.一级处理 && typeof(it.一级处理) == 'string') { //模板传了第二优先
                一级处理 = eval(it.一级处理);
            } else { //规则未写且模板未传
                一级处理 = {};
            }
        }
        try {
            if (it.自动初始分类) {
                if (typeof(it.初始分类链接处理) === 'string' && /return/.test(it.初始分类链接处理)) {
                    it.初始分类链接处理 = eval(it.初始分类链接处理);
                }
                初始分类页(it.初始分类链接定位, it.初始分类链接处理);
            }
            true_url = 获取正确链接();
            var html = 获取源码(true_url);
            let cates = 打造动态分类(it.动态分类列表, {
                源码: html,
                关闭折叠: it.关闭折叠
            });
            设置(cates, it.设置倒放);
            一级(parStr, it.是否有二级, cates, it.免责, html);
            if (parseInt(page) === 1) {
                log('一级匹配成功,第' + i + '个模板:【' + it.名称 + '】');
            }
            匹配成功 = true;
            break;
        } catch (e) {
            log('一级模板【' + it.名称 + '】匹配失败,尝试下一个模板\n' + e.message)
        }
    }
    if (!匹配成功) {
        throw new Error("全部已有一级模板匹配失败,请扩充模板库");
    }
}

var 自动一级 = function (success_flag, cates, html, test_mode) { //不带分类的一级
    if (typeof(page) === 'undefined' || !page) {
        page = MY_PAGE || 1;
    }
    menucates = 初始化菜单();
    cesy = getMyVar("cesy", "");
    if (cesy != "*") {
        if (parseInt(page) == 1) {

            // cates = cates.filter((cate) => cate.col_type != "scroll_button");
            cates = menucates.concat(cates);
            true_url = getHome(MY_URL);
            log(true_url);
            html = fetch(true_url);
            //setPreResult(cates);
            cates = cates.concat(自动首页(null, [], html, true));
            setResult(cates);
        }
        //setResult(cates);
    } else {
        success_flag = success_flag || 'html'; //默认不传就是html，意思是只要源码含有Html标签都算是对的
        test_mode = test_mode || false; //测试模式
        依赖检测(); //一级界面检测依赖
        MY_URL = MY_URL.replace("hiker://empty##", "").split('#')[0];
        var 规则处理 = typeof(一级处理) !== 'undefined';
        if (!html) {
            log('首页链接:' + MY_URL);
        }
        var 匹配成功 = false;
        var obj = {};
        let muban = JSON.parse(getMyVar('yjmuban', '{}')); //获取上次一级模板
        let idex = 一级定位模板.findIndex(it => it.名称 === muban.名称); // 找到索引
        if (idex > -1) {
            let delItem = 一级定位模板.splice(idex, 1); // 删除
            一级定位模板.unshift(delItem[0]); //放到最头上
        }
        let ck_id = `cookie.${MY_RULE.title}`;
        if (!getMyVar('cookie') || success_flag !== 'html') { //如果没有cookie则主动获取cookie,有搜索验证结果的必然更新cookie
            let cookie = 获取ck(ck_id);
            putMyVar('cookie', cookie);
        }
        // log('cookie:'+getMyVar('cookie'));
        // let cookie='searchneed=ok';
        html = html && typeof(html) === 'string' && /html/.test(html) ? html : 获取源码(MY_URL); //源码只获取一次
        cates = cates && Array.isArray(cates) ? cates : [];
        var mactchFlag = new RegExp(success_flag); //变量构造正则表达式
        if (!mactchFlag.test(html)) { //没有包含成功标志的东西就跳道长验证
            log('开始道长验证通杀:' + ck_id);
            道长验证码('网站搜索异常，点此手动处理验证或看网页挂了么', MY_URL, '', ck_id);
        } else {
            for (let i in 一级定位模板) {
                let it = 一级定位模板[i];
                let parStr = it.解析;
                if (!规则处理) { //规则没写处理
                    if (it.一级处理 && typeof(it.一级处理) == 'string') { //模板传了第二优先
                        一级处理 = eval(it.一级处理);
                    } else { //规则未写且模板未传
                        一级处理 = {};
                    }
                }
                一级处理.自动匹配 = true;
                leiFlag = true;
                try {
                    let cateLei = [];
                    if (parseInt(page) === 1) {
                        if (it.自动初始分类 && 一级处理.自动初始分类) {
                            if (typeof(it.初始分类链接处理) === 'string' && /return/.test(it.初始分类链接处理)) {
                                it.初始分类链接处理 = eval(it.初始分类链接处理);
                            }
                            初始分类页(it.初始分类链接定位, it.初始分类链接处理);
                            true_url = (typeof(true_url) !== 'undefined' && true_url) ? true_url : getMyVar('header.url', MY_URL);
                            log(true_url);
                            var html = 获取源码(true_url);
                            cateLei = 打造动态分类(it.动态分类列表, {
                                源码: html,
                                关闭折叠: it.关闭折叠
                            });
                            // log(cateLei);
                            leiFlag = cateLei.length > 0 ? true : false;
                            cateLei = cateLei.concat(cates);
                        } else {
                            cateLei = cates;
                        }
                    }
                    // log(cateLei);
                    var cateData = [];
                    cateData = 一级(parStr, it.是否有二级, cateData, it.免责, html, true); //定义个变量接受返回值列表
                    if (parseInt(page) === 1 && !test_mode) {
                        cates = menucates.concat(cateLei);
                    } else if (parseInt(page) === 1 && test_mode) {
                        cates = cateLei.concat(cates);
                    }
                    if (typeof(it.自动初始分类) == "undefined") {
                        leiFlag = false;
                        it_tmep = JSON.parse(JSON.stringify(it));
                        it_tmep["自动初始分类"] = it["自动初始分类"] || true;
                        it_tmep["初始分类链接处理"] = it["初始分类链接处理"] || "\n(u) => {}\n";
                        it_tmep["初始分类链接定位"] = it["初始分类链接定位"] || it["动态分类列表"][0]["一级分类"] + "&&a,1&&href"
                            putMyVar("yjmuban_temp", JSON.stringify(it_tmep));
                    } else {
                        cates = cates.concat(cateData);
                    }
                    匹配成功 = true;
                    obj = it;
                    putMyVar('yjmuban', JSON.stringify(obj)); //匹配成功保存上次匹配的模板,下次匹配直接排第一个
                    log('一级模板匹配成功,第' + i + '个模板:【' + it.名称 + '】');
                    break;
                } catch (e) {
                    log('一级模板【' + it.名称 + '】匹配失败,尝试下一个模板\n' + e.message)
                }
            }
            if (匹配成功 && test_mode) {
                return cates;
            }
            if (匹配成功 && leiFlag) {
                //log(cates);
                setResult(cates);
            } else {
                // cates = menucates.concat(cates);
                title = 匹配成功 ? "请修改下面的例子,适配自动分类" : "全部已有一级模板匹配失败,请扩充模板库\n请按照下面例子,扩充一级模板库";
                cates.push({
                    title: title,
                    desc: "一个一级模板例子!",
                    col_type: "text_center_1"
                });
                cates.push({
                    title: "代码查看",
                    desc: "结果输出",
                    col_type: 'input',
                    extra: {
                        // titleVisible: false,
                        highlight: true,
                        height: -1,
                        defaultValue: getMyVar("yjmuban_temp", JSON.stringify(一级定位模板[0])),
                        type: "textarea",
                        onChange: 'putMyVar("yjmuban_temp", input)'
                    },
                    url: "",
                });
                cates.push({
                    title: "生成导入口令",
                    desc: "口令结果",
                    col_type: "text_2",
                    extra: {
                        id: "yjmuban_temp_2"
                    },
                    url: $().lazyRule(() => {
                        input = getMyVar("yjmuban_temp", "{}");
                        putMyVar("yjmuban", input);
                        let rule_exits = fetch('hiker://home@BILL模板');
                        rule_exits = rule_exits !== 'null' && rule_exits !== '';
                        if (!rule_exits) {
                            return $('未安装[BILL模板]小程序，无法下载资源,现在安装?').confirm(() => {
                                let ruleHead = '海阔视界首页频道规则【BILL模板】￥home_rule_url￥';
                                let rulecode = 'rule://' + base64Encode(ruleHead + 'https://gitlab.com/mao4284120/dr/-/raw/master/json/bill.json');
                                return rulecode
                            });
                        } else {
                            input = sharePaste(input);
                            log(input);
                            if (input.search("error") > -1) {
                                input = sharePaste(getMyVar("yjmuban_temp", "{}"), "云剪贴板5");
                            }
                            input = "海阔视界「BILL模板 - 共 1 条」，复制整条口令自动导入$" + base64Encode(input) + "$b$一级模板@import=js:$.require('import?rule='+\"BILL模板\")(input)";
                            toast("云口令生成成功!");
                            return "copy://" + input;
                        }
                    })
                })
                cates.push({
                    title: "一级模板测试",
                    desc: "测试结果",
                    col_type: "text_2",
                    extra: {
                        id: "yjmuban_temp_1"
                    },
                    url: $().lazyRule(() => {
                        clearMyVar("header.url");
                        deleteItemByCls("yimuban_temp");
                        it = JSON.parse(getMyVar("yjmuban_temp", "{}"));
                        //log(it);
                        log(MY_URL);
                        require(config.自动匹配);
                        //require("hiker://page/自动匹配")
                        page = 1;
                        if (typeof(it.初始分类链接处理) === 'string' && /return/.test(it.初始分类链接处理)) {
                            it.初始分类链接处理 = eval(it.初始分类链接处理);
                        }
                        初始分类页(it.初始分类链接定位, it.初始分类链接处理);
                        true_url = (typeof(true_url) !== 'undefined' && true_url) ? true_url : getMyVar('header.url', MY_URL);
                        //.replace(/vodtype\/(\w+)-1.html/,"vodshow/$1--------1---.html");
                        log("true_url = " + true_url);
                        var html = 获取源码(true_url);
                        cateLei = 打造动态分类(it.动态分类列表, {
                            源码: html,
                            关闭折叠: true
                        });

                        cateData = [];
                        cateData = 一级(it.解析, it.是否有二级, cateData, it.免责, html, true);
                        cateDatas = cateLei.concat(cateData);
                        cateDatas = cateDatas.map((cateData) => {
                            cateData["extra"] = {
                                cls: "yjmuban_temp"
                            };
                            return cateData;
                        });
                        if (cateDatas.length == 0) {
                            deleteItemByCls("yjmuban_temp");
                            return "toast://模板测试有误"
                        } else {
                            addItemAfter("yjmuban_temp_1", cateDatas);
                            return "toast://模板测试成功，快去生成导入口令吧"
                        }
                    })
                })
                //throw new Error("全部已有一级模板匹配失败,请扩充模板库");
                setResult(cates);
            }
        }
    }
}

var 自动搜索 = function (success_flag) {
    success_flag = success_flag || 'html'; //默认不传就是html，意思是只要源码含有Html标签都算是对的
    // success_flag = success_flag||'搜索结果';
    依赖检测();
    if (typeof(html) !== 'string' || !/html/.test(html)) {
        MY_URL = 获取搜索链接();
        // html = 获取源码(MY_URL);//搜索源码只获取一次
        html = getCode();
    }
    var 规则处理 = typeof(一级处理) !== 'undefined';
    var page = MY_PAGE;
    var 匹配成功 = false;
    var obj = {};
    let muban = JSON.parse(getMyVar('ssmuban', '{}')); //获取上次搜索模板
    let idex = 搜索定位模板.findIndex(it => it.名称 === muban.名称); // 找到索引
    if (idex > -1) {
        let delItem = 搜索定位模板.splice(idex, 1); // 删除
        搜索定位模板.unshift(delItem[0]); //放到最头上
    }
    let ck_id = `cookie.${MY_RULE.title}`;
    if (!getMyVar('cookie') || success_flag !== 'html') { //如果没有cookie则主动获取cookie,有搜索验证结果的必然更新cookie
        let cookie = 获取ck(ck_id);
        putMyVar('cookie', cookie);
    }
    // log('cookie:'+getMyVar('cookie'));
    // let cookie='searchneed=ok';
    var mactchFlag = new RegExp(success_flag); //变量构造正则表达式
    if (!mactchFlag.test(html)) { //没有包含搜索成功标志的东西就跳道长验证
        log('开始道长验证通杀:' + ck_id);
        道长验证码('网站搜索异常，点此手动处理验证或看网页挂了么', MY_URL, '', ck_id);
    } else {
        for (let i in 搜索定位模板) {
            let it = 搜索定位模板[i];
            let parStr = it.解析;
            if (!规则处理) { //规则没写处理
                if (it.一级处理 && typeof(it.一级处理) == 'string') { //模板传了第二优先
                    一级处理 = eval(it.一级处理);
                } else { //规则未写且模板未传
                    一级处理 = {};
                }
            }
            一级处理.自动匹配 = true;
            try {
                一级(parStr, it.是否有二级, [], it.免责, html);
                if (parseInt(page) === 1) {
                    log('搜索匹配成功,第' + i + '个模板:【' + it.名称 + '】');
                }
                匹配成功 = true;
                obj = it;
                putMyVar('ssmuban', JSON.stringify(obj)); //匹配成功保存上次匹配的模板,下次匹配直接排第一个
                break;
            } catch (e) {
                log('搜索模板【' + it.名称 + '】匹配失败,尝试下一个模板\n' + e.message)
            }
        }
        if (!匹配成功) {
            d = [];
            d.push({
                title: "全部已有搜索模板匹配失败,请扩充模板库\n请按照下面例子,扩充搜索模板库",
                desc: "一个搜索模板例子!",
                col_type: "text_center_1",
            });
            d.push({
                title: "点击,扩充搜索模板库",
                desc: "一个搜索模板例子!",
                col_type: "text_3",
                url: $(MY_URL).rule(() => {
                    cates = [];
                    cates.push({
                        title: "代码查看",
                        desc: "结果输出",
                        col_type: 'input',
                        extra: {
                            // titleVisible: false,
                            highlight: true,
                            height: -1,
                            defaultValue: getMyVar("ssmuban_temp", JSON.stringify(搜索定位模板[0])),
                            type: "textarea",
                            onChange: 'putMyVar("ssmuban_temp", input)'
                        },
                        url: "",
                    });
                    cates.push({
                        title: "生成导入口令",
                        desc: "口令结果",
                        col_type: "text_2",
                        extra: {
                            id: "ssmuban_temp_2"
                        },
                        url: $().lazyRule(() => {
                            input = getMyVar("ssmuban_temp", "{}");
                            putMyVar("ssmuban", input);
                            let rule_exits = fetch('hiker://home@BILL模板');
                            rule_exits = rule_exits !== 'null' && rule_exits !== '';
                            if (!rule_exits) {
                                return $('未安装[BILL模板]小程序，无法下载资源,现在安装?').confirm(() => {
                                    let ruleHead = '海阔视界首页频道规则【BILL模板】￥home_rule_url￥';
                                    let rulecode = 'rule://' + base64Encode(ruleHead + 'https://gitlab.com/mao4284120/dr/-/raw/master/json/bill.json');
                                    return rulecode
                                });
                            } else {
                                input = sharePaste(input);
                                log(input);
                                if (input.search("error") > -1) {
                                    input = sharePaste(getMyVar("ssmuban_temp", "{}"), "云剪贴板5");
                                }
                                input = "海阔视界「BILL模板 - 共 1 条」，复制整条口令自动导入$" + base64Encode(input) + "$b$搜索模板@import=js:$.require('import?rule='+\"BILL模板\")(input)";
                                toast("云口令生成成功!");
                                return "copy://" + input;
                            }
                        })
                    })
                    cates.push({
                        title: "搜索模板测试",
                        desc: "测试结果",
                        col_type: "text_2",
                        extra: {
                            id: "ssmuban_temp_1"
                        },
                        url: $().lazyRule(() => {
                            it = JSON.parse(getMyVar("ssmuban_temp", "{}"));
                            //log(it);
                            //log(MY_URL);
                            require(config.自动匹配);
                            //require("hiker://page/自动匹配")
                            MY_PAGE = 1;
                            true_url = 获取正确链接();
                            var html = 获取源码(true_url);

                            cateDatas = [];
                            cateDatas = 一级(it.解析, it.是否有二级, cateDatas, it.免责, html, true);
                            cateDatas = cateDatas.map((cateData) => {
                                cateData["extra"] = {
                                    cls: "ssmuban_temp"
                                };
                                return cateData;
                            });
                            if (cateDatas.length == 0) {
                                deleteItemByCls("ssmuban_temp");
                                return "toast://模板测试有误"
                            } else {
                                addItemAfter("ssmuban_temp_1", cateDatas);
                                return "toast://搜索模板测试成功，快去生成导入口令吧"
                            }
                        })
                    });
                    setResult(cates);
                })
            });
            setResult(d);
            //throw new Error("全部已有搜索模板匹配失败,请扩充模板库");
        }
    }
}

var 自动二级 = function (lazy, extra) {
    extra = extra || {};
    type = extra.type || "";
    let lazy2 = `var 解析=${$.stringify(解析)};var lazy=` + function (input) {
        var 全局免嗅 = function (input) {
            try {
                let ret = request(input).match(/var player_(.*?)=(.*?)</)[2];
                let url = JSON.parse(ret).url;
                if (/\.m3u8|\.mp4/.test(url)) {
                    return 解析.是否视频(url)
                } else if (!/http/.test(url) && !/\//.test(url)) {
                    try {
                        url = unescape(base64Decode(url));
                        if (/http/.test(url)) {
                            return 解析.纯通免(url)
                        } else {
                            return 解析.纯通免(input)
                        }
                    } catch (e) {
                        return 解析.纯通免(input)
                    }
                } else {
                    return 解析.纯通免(input)
                }
            } catch (e) {
                return 解析.纯通免(input)
            }
        };
        return 全局免嗅(input)
    }
    .toString();
    let path = 'hiker://files/cache/js/自动匹配免嗅.js';
    saveFile(path, lazy2, 0);
    if (type == "") {
        lazy = lazy || $("").lazyRule((path) => {
            // return input+request(path);
            eval(request(path));
            return lazy(input);
        }, path);
    }
    // var 动态最新章节=true;
    // var 倒序=false;
    var 匹配成功 = false;
    回调模式 = (!(typeof(回调模式) === 'undefined' || !回调模式));
    var 规则处理 = typeof(二级处理) !== 'undefined';
    var obj = {};
    var result = []; //匹配后的二级结果
    let muban = JSON.parse(getMyVar('muban', '{}')); //获取上次模板
    let idex = 二级定位模板.findIndex(it => it.名称 === muban.名称); // 找到索引
    if (idex > -1) {
        let delItem = 二级定位模板.splice(idex, 1); // 删除
        二级定位模板.unshift(delItem[0]); //放到最头上
    }
    for (let i in 二级定位模板) {
        let it = 二级定位模板[i];
        let parse = it.解析;
        动态最新章节 = !!it.动态最新章节;
        if (type != "" && !/小说|漫画/.test(it.type || "")) {
            log('二级模板【' + it.名称 + '】不是' + type + '模板,尝试下一个模板\n');
            continue;
        } else if (type != "" && /小说|漫画/.test(it.type || "")) {
            log("进入小说漫画,模板匹配");
            下载 = type;
            阅读模式 = true;
            定位 = it.定位;
            定位.标题 = typeof(定位.标题) == "function" ? 定位.标题 : eval(定位.标题);
            定位.内容 = typeof(定位.内容) == "function" ? 定位.内容 : eval(定位.内容);
            定位.链接 = typeof(定位.链接) == "function" ? 定位.链接 : typeof(定位.链接) == "undefined" ? "" : eval(定位.链接);
            lazy3 = type == "小说" ? 小说(定位, it.编码) : type == "漫画" ? null : lazy;
            lazy = lazy || lazy3;
        }
        if (!规则处理) { //规则没写处理
            if (it.二级处理 && typeof(it.二级处理) == 'string') { //模板传了第二优先
                二级处理 = eval(it.二级处理);
            } else { //规则未写且模板未传
                二级处理 = {};
            }
        }
        try {
            result = 二级(parse, lazy, it.需要魔断, true);
            log('二级匹配成功,第' + i + '个模板:【' + it.名称 + '】');
            匹配成功 = true;
            obj = it;
            putMyVar('muban', JSON.stringify(obj)); //匹配成功保存上次匹配的模板,下次匹配直接排第一个
            break;
        } catch (e) {
            log('二级模板【' + it.名称 + '】匹配失败,尝试下一个模板\n' + e.message);
            // config.html='';//匹配失败清除缓存的页面
            // putMyVar('lastUrl','');
        }
    }
    if (匹配成功 && 回调模式) {
        return result;
    }
    if (!匹配成功) {
        cates = [];
        cates.push({
            title: "全部已有二级模板匹配失败,请扩充模板库\n请按照下面例子,扩充二级模板库",
            desc: "一个二级模板例子!",
            col_type: "text_center_1"
        });
        cates.push({
            title: "代码查看",
            desc: "结果输出",
            col_type: 'input',
            extra: {
                // titleVisible: false,
                highlight: true,
                height: -1,
                defaultValue: getMyVar("ejmuban_temp", JSON.stringify(二级定位模板[0])),
                type: "textarea",
                onChange: 'putMyVar("ejmuban_temp", input)'
            },
            url: "",
        });
        cates.push({
            title: "生成导入口令",
            desc: "口令结果",
            col_type: "text_2",
            extra: {
                id: "ejmuban_temp_2"
            },
            url: $().lazyRule(() => {
                input = getMyVar("ejmuban_temp", "{}");
                putMyVar("ejmuban", input);
                let rule_exits = fetch('hiker://home@BILL模板');
                rule_exits = rule_exits !== 'null' && rule_exits !== '';
                if (!rule_exits) {
                    return $('未安装[BILL模板]小程序，无法下载资源,现在安装?').confirm(() => {
                        let ruleHead = '海阔视界首页频道规则【BILL模板】￥home_rule_url￥';
                        let rulecode = 'rule://' + base64Encode(ruleHead + 'https://gitlab.com/mao4284120/dr/-/raw/master/json/bill.json');
                        return rulecode
                    });
                } else {
                    input = sharePaste(input);
                    log(input);
                    if (input.search("error") > -1) {
                        input = sharePaste(getMyVar("ejmuban_temp", "{}"), "云剪贴板5");
                    }
                    input = "海阔视界「BILL模板 - 共 1 条」，复制整条口令自动导入$" + base64Encode(input) + "$b$二级模板@import=js:$.require('import?rule='+\"BILL模板\")(input)";
                    toast("云口令生成成功!");
                    return "copy://" + input;
                }
            })
        })
        cates.push({
            title: "二级模板测试",
            desc: "测试结果",
            col_type: "text_2",
            extra: {
                id: "ejmuban_temp_1"
            },
            url: $().lazyRule((lazy) => {
                deleteItemByCls("ejmuban_temp");
                it = JSON.parse(getMyVar("ejmuban_temp", "{}"));
                log(MY_URL);
                require(config.自动匹配);
                MY_PAGE = 1;
                cateData = [];
                MY_PARAMS = {};
                cateDatas = 二级(it.解析, lazy, it.需要魔断, true);
                cateDatas = cateDatas.map((cateData) => {
                    cateData["extra"] = {
                        cls: "ejmuban_temp"
                    };
                    return cateData;
                });
                if (cateDatas.length == 0) {
                    deleteItemByCls("ejmuban_temp");
                    return "toast://模板测试有误"
                } else {
                    addItemAfter("ejmuban_temp_1", cateDatas);
                    return "toast://模板测试成功，快去生成导入口令吧"
                }
            }, lazy)
        })
        //throw new Error("全部已有二级模板匹配失败,请扩充模板库");
        setResult(cates);
    }
}

function banner(title, start, arr, data, cfg) {
    let id = title + 'lunbo';
    var rnum = Math.floor(Math.random() * data.length);
    var item = data[rnum];
    putMyVar('rnum', rnum);
    let time = 5000;
    let col_type = 'pic_1_card';
    let color = "white";
    let desc = '';
    if (cfg != undefined) {
        time = cfg.time ? cfg.time : time;
        col_type = cfg.col_type ? cfg.col_type : col_type;
        desc = cfg.desc ? cfg.desc : desc;
    }

    arr.push({
        col_type: col_type,
        img: item.img,
        desc: desc,
        title: item.title,
        url: item.url,
        extra: {
            id: id + 'bar',
            title: item.title,
            img: item.img,
            longClick: [{
                    title: '⚙️设置',
                    js: $.toString(() => {
                        let d = [];
                        require(config.自动匹配);
                        MY_PAGE = 1;
                        设置(d);
                        return d[0].url;
                    })
                }, {
                    title: '⏰最近',
                    js: $.toString(() => {
                        return $('#noLoading#').lazyRule((title) => {
                            let rules = getLastRules(20);
                            let sel_rules = rules.map(it => it.title);
                            return $(sel_rules, 2, '请选一个最近使用的小程序').select((title) => {
                                return "hiker://home@" + input
                            }, title)
                        })
                    })
                }, {
                    title: '⭐收藏',
                    js: $.toString(() => {
                        return "hiker://collection?group=####九石"
                    })

                }, {
                    title: '🔖书签',
                    js: $.toString(() => {
                        var html = [];
                        eval('html=' + request('hiker://bookmark', {}));
                        var nameurl = [];
                        for (var i = 0; i < html.length; i++) {
                            nameurl[i] = {
                                'title': '',
                                'url': ''
                            };
                            nameurl[i].title = html[i].title;
                            nameurl[i].url = html[i].url;
                        };
                        const namet = nameurl.map(name => name.title);
                        return $(namet, 1, '书签速览').select((nameurl) => {
                            let url_j = nameurl.findIndex(name => name.title == input);
                            return "web://" + nameurl[url_j].url
                        }, nameurl)
                    })
                }
            ]
        }
    })

    if (start == false || getMyVar('benstart', 'true') == 'false') {
        unRegisterTask(id)
        return
    }

    let obj = {
        data: data,
    };

    registerTask(id, time, $.toString((obj, id) => {
            var data = obj.data;
            var rum = getMyVar('rnum');

            var i = Number(getMyVar('banneri', '0'));
            if (rum != '') {
                i = Number(rum) + 1
                    clearMyVar('rnum')
            } else {
                i = i + 1;
            }

            if (i > data.length - 1) {
                i = 0
            }
            var item = data[i];
            try {
                updateItem(id + 'bar', {
                    title: item.title,
                    img: item.img,
                    url: item.url,
                    extra: {
                        title: item.title,
                        img: item.img,
                        longClick: [{
                                title: '⚙️设置',
                                js: $.toString(() => {
                                    let d = [];
                                    require(config.自动匹配);
                                    MY_PAGE = 1;
                                    设置(d);
                                    return d[0].url;
                                })
                            }, {
                                title: '⏰最近',
                                js: $.toString(() => {
                                    return $('#noLoading#').lazyRule((title) => {
                                        let rules = getLastRules(20);
                                        let sel_rules = rules.map(it => it.title);
                                        return $(sel_rules, 2, '请选一个最近使用的小程序').select((title) => {
                                            return "hiker://home@" + input
                                        }, title)
                                    })
                                })
                            }, {
                                title: '⭐收藏',
                                js: $.toString(() => {
                                    return "hiker://collection?group=####九石"
                                })

                            }, {
                                title: '🔖书签',
                                js: $.toString(() => {
                                    var html = [];
                                    eval('html=' + request('hiker://bookmark', {}));
                                    var nameurl = [];
                                    for (var i = 0; i < html.length; i++) {
                                        nameurl[i] = {
                                            'title': '',
                                            'url': ''
                                        };
                                        nameurl[i].title = html[i].title;
                                        nameurl[i].url = html[i].url;
                                    };
                                    const namet = nameurl.map(name => name.title);
                                    return $(namet, 1, '书签速览').select((nameurl) => {
                                        let url_j = nameurl.findIndex(name => name.title == input);
                                        return "web://" + nameurl[url_j].url
                                    }, nameurl)
                                })
                            }
                        ]
                    }

                })
            } catch (e) {
                log(e.message)
                unRegisterTask(id)
            }
            putMyVar('banneri', i);

        }, obj, id))
}
var 自动首页 = function (success_flag, cates, html, test_mode) { //不带分类的一级
    success_flag = success_flag || 'html'; //默认不传就是html，意思是只要源码含有Html标签都算是对的
    test_mode = test_mode || false; //测试模式
    依赖检测(); //一级界面检测依赖
    MY_URL = MY_URL.replace("hiker://empty##", "").split('#')[0];
    var 规则处理 = typeof(一级处理) !== 'undefined';
    if (!html) {
        log('首页链接:' + MY_URL);
    }
    var page = MY_PAGE;
    var 匹配成功 = false;
    var obj = {};
    let muban = JSON.parse(getMyVar('symuban', '{}')); //获取上次一级模板
    let idex = 首页定位模板.findIndex(it => it.名称 === muban.名称); // 找到索引
    if (idex > -1) {
        let delItem = 首页定位模板.splice(idex, 1); // 删除
        首页定位模板.unshift(delItem[0]); //放到最头上
    }
    let ck_id = `cookie.${MY_RULE.title}`;
    if (!getMyVar('cookie') || success_flag !== 'html') { //如果没有cookie则主动获取cookie,有搜索验证结果的必然更新cookie
        let cookie = 获取ck(ck_id);
        putMyVar('cookie', cookie);
    }
    // log('cookie:'+getMyVar('cookie'));
    // let cookie='searchneed=ok';
    html = html && typeof(html) === 'string' && /html/.test(html) ? html : 获取源码(MY_URL); //源码只获取一次
    cates = cates && Array.isArray(cates) ? cates : [];
    var mactchFlag = new RegExp(success_flag); //变量构造正则表达式
    if (!mactchFlag.test(html)) { //没有包含成功标志的东西就跳道长验证
        log('开始道长验证通杀:' + ck_id);
        道长验证码('网站搜索异常，点此手动处理验证或看网页挂了么', MY_URL, '', ck_id);
    } else {
        for (let i in 首页定位模板) {
            let it = 首页定位模板[i];
            let parStr = it.解析 || "";
            parStr = parStr.split("$$")[0];
            title = it.标题 || "";
            titles = title.split("$$");
            if (!规则处理) { //规则没写处理
                if (it.一级处理 && typeof(it.一级处理) == 'string') { //模板传了第二优先
                    一级处理 = eval(it.一级处理);
                } else { //规则未写且模板未传
                    一级处理 = {};
                }
            }
            一级处理.自动匹配 = true;
            try {
                // var result = 一级(parStr, it.是否有二级, [], it.免责, html, true); //定义个变量接受返回值列表
                var result = 一级(parStr, it.是否有二级, [], it.免责, html, true); //定义个变量接受返回值列表
                pdfh(html, title);
                if (parseInt(page) === 1) {
                    log('首页模板匹配成功,第' + i + '个模板:【' + it.名称 + '】');
                }
                匹配成功 = true;
                obj = it;
                putMyVar('symuban', JSON.stringify(obj)); //匹配成功保存上次匹配的模板,下次匹配直接排第一个
                break;
            } catch (e) {
                log('首页模板【' + it.名称 + '】匹配失败,尝试下一个模板\n' + e.message)
            }
        }
        if (匹配成功) {
            it = JSON.parse(getMyVar("symuban", "{}"));
            cates = buildTopData(it, html);
        }
        if (!匹配成功) {
            cates.push({
                title: "全部已有首页模板匹配失败,请扩充模板库\n请按照下面例子,扩充首页模板库",
                desc: "一个首页模板例子!",
                col_type: "text_1"
            });
            cates.push({
                title: "代码查看",
                desc: "结果输出",
                col_type: 'input',
                extra: {
                    // titleVisible: false,
                    highlight: true,
                    height: -1,
                    defaultValue: getMyVar("symuban_temp", JSON.stringify(首页定位模板[0])),
                    type: "textarea",
                    onChange: 'putMyVar("symuban_temp", input)'
                },
                url: "",
            });
            cates.push({
                title: "生成导入口令",
                desc: "口令结果",
                col_type: "text_2",
                extra: {
                    id: "symuban_temp_2"
                },
                url: $().lazyRule(() => {
                    input = getMyVar("symuban_temp", "{}");
                    putMyVar("symuban", input);
                    let rule_exits = fetch('hiker://home@BILL模板');
                    rule_exits = rule_exits !== 'null' && rule_exits !== '';
                    if (!rule_exits) {
                        return $('未安装[BILL模板]小程序，无法下载资源,现在安装?').confirm(() => {
                            let ruleHead = '海阔视界首页频道规则【BILL模板】￥home_rule_url￥';
                            let rulecode = 'rule://' + base64Encode(ruleHead + 'https://gitlab.com/mao4284120/dr/-/raw/master/json/bill.json');
                            return rulecode
                        });
                    } else {
                        input = sharePaste(input);
                        log(input);
                        if (input.search("error") > -1) {
                            input = sharePaste(getMyVar("symuban_temp", "{}"), "云剪贴板5");
                        }
                        input = "海阔视界「BILL模板 - 共 1 条」，复制整条口令自动导入$" + base64Encode(input) + "$b$首页模板@import=js:$.require('import?rule='+\"BILL模板\")(input)";
                        toast("云口令生成成功!");
                        return "copy://" + input;
                    }
                })
            })
            cates.push({
                title: "首页模板测试",
                desc: "测试结果",
                col_type: "text_2",
                extra: {
                    id: "symuban_temp_1"
                },
                url: $().lazyRule(() => {
                    deleteItemByCls("symuban_temp");
                    it = JSON.parse(getMyVar("symuban_temp", "{}"));
                    //log(it);
                    log(MY_URL);
                    require(config.自动匹配);
                    //require("hiker://page/自动匹配")
                    MY_HOME = getHome(MY_URL);
                    html = fetch(MY_HOME);
                    try {
                        cateDatas = buildTopData(it, html);
                        // cateDatas = cateDatas.map((cateData) => {
                        //     cateData["extra"] = {
                        //         cls: "symuban_temp"
                        //     };
                        //     return cateData;
                        // });
                    } catch (e) {
                        log(e.toString());
                        return "toast://模板测试有误"
                    }
                    cateDatas.push({
                        col_type: "big_blank_block"
                    }, {
                        col_type: "line"
                    }, {
                        col_type: "big_blank_block"
                    }, {
                        title: "测试结束",
                        col_type: "text_center_1"
                    });
                    addItemAfter("symuban_temp_1", cateDatas);
                    return "toast://模板测试成功，快去生成导入口令吧"
                })
            })
        }
        if (test_mode) {
            return cates;
        } else {
            setResult(cates);
        }

    }
}

var 通用标题处理 = function (input) {
    return input;
}

function buildTopData(it, html) {
    cates = [];
    title = it.标题 || "";
    titles = title.split("$$");
    parStrs = it.解析.split("$$");
    cateDatas = [];
    标题处理 = (typeof(标题处理) == "undefined" || !标题处理) ? 通用标题处理 : 标题处理;
    标题处理索引 = typeof(标题处理索引) == "undefined" ? [] : 标题处理索引;
    var 规则处理 = typeof(一级处理) !== 'undefined';
    if (!规则处理) { //规则没写处理
        if (it.一级处理 && typeof(it.一级处理) == 'string') { //模板传了第二优先
            一级处理 = eval(it.一级处理);
        } else { //规则未写且模板未传
            一级处理 = {};
        }
    }
    if (标题处理索引.length == 0) {
        for (var i = 0; i < titles.length; i++) {
            标题处理索引.push(i);
        }
    }
    log(标题处理索引)
    if (一级处理.标题处理) {
        titles = 一级处理.标题处理(html);
    } else {
        titles = titles.filter((title, index) => {
            return 标题处理索引.indexOf(index) > -1;
        }).map((title, index) => {
            try {
                return 标题处理(pdfh(html, title), index);
            } catch (e) {
                log(e.toString())
            }
            return null;
        });
    }
    log(titles);
    haibao = [];
    //log(it.海报);
    if (it.海报) {
        try {
            if (一级处理.海报) {
                haibaoData = 一级处理.海报(html);
            } else {
                haibaoData = 一级(it.海报, it.是否有二级, haibao, it.免责, html, true);
            }
            images = haibaoData.map(cate => {
                return {
                    title: cate.title.trim(), // 去除可能的空白字符
                    img: cate.pic_url.trim().replace("@Referer=", ""),
                    desc: cate.title.trim(),
                    url: cate.url.trim()
                }
            });

            banner(MY_RULE.title, true, haibao, images, {
                time: 5000,
                col_type: 'card_pic_1',
                desc: '0'
            })
        } catch (e) {
            log(e.toString());
        }
    }
    log(haibao);
    if (一级处理.列表处理) {
        cateDatas = 一级处理.列表处理(html);
    } else {
        parStrs.map((parStr, index) => {
            try {
                cateData = [];
                cateData = 一级(parStr, it.是否有二级, cateData, it.免责, html, true);
                cateData = cateData.map((c) => {
                    c["extra"] = {
                        cls: MY_HOME + "_" + index
                    };
                    return c;
                });
                cateDatas.push(cateData);
            } catch (e) {
                log(e.toString());
            }
        });
    }
    //log(cateDatas);
    if (haibao.length == 0) {
        cateData = cateDatas[0];
        images = cateData.map(cate => {
            return {
                title: cate.title.trim(), // 去除可能的空白字符
                img: cate.pic_url.trim().replace("@Referer=", ""),
                desc: cate.title.trim(),
                url: cate.url.trim()
            }
        });

        banner(MY_RULE.title, true, haibao, images, {
            time: 5000,
            col_type: 'card_pic_1',
            desc: '0'
        })
    }
    // cates = cates.concat(haibao);
    cateDatas.map((cateData, index) => {
        t = titles[index];
        cates.push({
            title: getMyVar(t, "1") == "1" ? '<b><span style="color: #FA7298">' + t + '📬</span></b>' : t + '📪',
            url: $().lazyRule((cateData, t, mid, cid) => {
                if (getMyVar(t, "1") === "1") {
                    updateItem(cid, {
                        title: t + '📪'
                    });
                    deleteItemByCls(mid);
                } else {
                    updateItem(cid, {
                        title: '<b><span style="color: #FA7298">' + t + '📬</span></b>'
                    });
                    addItemAfter(cid, cateData);
                }
                putMyVar(t, getMyVar(t, "1") === "1" ? "0" : "1");
                return "hiker://empty"
            }, cateData, t, MY_HOME + "_" + index, t + "_" + index),
            pic_url: "https://hikerfans.com/more/59.png",
            col_type: "avatar",
            extra: {
                id: t + "_" + index
            }
        });
        if (getMyVar(t, "1") == "1") {
            cates = cates.concat(cateData);
        }
    });
    weekDatas = [];
    if (一级处理.周报) {
        weekDatas = 一级处理.周报(html);
    } else if (it.周报) {
        weeks = it.周报.split("$$");
        weekDatas = weeks.map((week, index) => {
            weekData = [];
            try {
                weekData = 一级(week, it.是否有二级, weekData, it.免责, html, true);
                weekData = weekData.map(w => {
                    t = JSON.parse(JSON.stringify(w));
                    t["extra"] = {
                        cls: "zhoubao_data"
                    };
                    return t;
                });
            } catch (e) {
                log(e.toString());
                weekData = [];
            }
            return weekData;
        });
    }
    if (weekDatas.length == 0) {
        weekDatas = new Array(7);
        weekDatas.fill([]);
    }
    weekDatas = weekDatas.map((weekData, index) => {
        if (weekData.length == 0) {
            weekData = cateDatas[index] || cateDatas[cateDatas.length - 1];
            weekData = weekData.map(w => {
                t = JSON.parse(JSON.stringify(w));
                t["extra"] = {
                    cls: "zhoubao_data"
                };
                return t;
            });
        }
        return weekData;
    });
    //log(weekDatas);
    zhoubao = [];
    var weeks = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];

    function getWeekDate() {
        var now = new Date();
        var day = now.getDay();
        var week = weeks[day];
        //log(week)
        return week;
    }
    let now_Week = getWeekDate();
    t = "追剧周表";

    wDatas = weeks.map((it, windex) => {
        return {
            title: getMyVar('week', now_Week) === it ? '☀' + '““””<b><font color=#FA7298>' + it + '</font></b>' : it.replace('周', ''),
            url: $('#noLoading##noHistory#').lazyRule((it, weekData,now_Week) => {
                oldit = getMyVar("week",now_Week);
                //log(oldit);
                //log(it);
                putMyVar("week", it);
                updateItem(oldit, {
                    title: oldit.replace('周', '')
                })
                updateItem(it, {
                    title: '☀' + '““””<b><font color=#FA7298>' + it + '</font></b>'
                });
                if (weekData && weekData.length > 0) {
                    deleteItemByCls("zhoubao_data");
                    addItemAfter("周六", weekData);
                }
                return "hiker://empty"
            }, it, weekDatas[windex], now_Week),
            col_type: "scroll_button",
            extra: {
                id: it,
                cls: "zhoubao_week"
            }
        };
    });
    let chose_week = getMyVar('week', now_Week);
    var wkTonum = function (wk) {
        if (/周|星期/.test(wk)) {
            wk = wk.replace(/周|星期/, "");
        }
        var map = {
            "日": 0,
            "一": 1,
            "二": 2,
            "三": 3,
            "四": 4,
            "五": 5,
            "六": 6
        };
        return map[wk];
    }

    let wk_num = wkTonum(chose_week);

    zhoubao.push({
        title: getMyVar(t, "1") === "1" ? '<b><span style="color: #FA7298">' + t + '📬</span></b>' : t + '📪',
        url: $().lazyRule((weekData, t, wDatas, chose_week) => {
            putMyVar("week", chose_week);
            if (getMyVar(t, "1") === "1") {
                updateItem(t, {
                    title: t + '📪'
                });
                deleteItemByCls("zhoubao_data");
                deleteItemByCls("zhoubao_week");
            } else {
                updateItem(t, {
                    title: '<b><span style="color: #FA7298">' + t + '📬</span></b>'
                });
                addItemAfter(t, wDatas.concat(weekData));
            }
            putMyVar(t, getMyVar(t, "1") === "1" ? "0" : "1");
            return "hiker://empty"
        }, weekDatas[wk_num], t, wDatas, chose_week),
        pic_url: "https://hikerfans.com/more/59.png",
        col_type: "avatar",
        extra: {
            id: t
        }
    });
    if (getMyVar(t, "1") == "1") {
        zhoubao = zhoubao.concat(wDatas);
        nowDatas = weekDatas[wk_num];
        zhoubao = zhoubao.concat(nowDatas);
    }
    // haibao = cates.splice(0, 1);
    cates = zhoubao.concat(cates);
    cates = haibao.concat(cates);
    return cates;

}

var 初始化菜单 = function () {
    var cates = [];
    if (MY_PAGE == 1) {
        首页 = $().lazyRule(() => {
            putMyVar("cesy", "")

            clearMyVar("mySou");
            refreshPage();
            return "toast://切换首页"
        });
        分类 = $().lazyRule(() => {
            putMyVar("cesy", "*")

            clearMyVar("mySou");
            refreshPage();
            return "toast://切换分类"
        });
        追更 = $().rule(() => {
            require("https://gitlab.com/mao4284120/dr/-/raw/master/js//Wp.js");
        });
        search_url = /^https?/.test(MY_RULE.search_url) ? MY_RULE.search_url : getHome(MY_URL) + MY_RULE.search_url;
        搜索 = $(search_url).rule((page) => {
            d = [];
            d.push({
                title: "搜索",
                desc: "搜索结果",
                col_type: "input",
                url: $.toString(() => {
                    putMyVar("mySou", input);
                    refreshPage();
                    return "toast://搜索中" + input;
                }),
                extra: {
                    defaultValue: getMyVar("mySou", ""),
                    onChange: 'putMyVar("mySou",input)'
                }
            });
            require(config.自动匹配);
            一级处理 = {
                链接(input) {
                    return $(input).rule(() => {
                        require(config.自动匹配);
                        自动二级();
                    })
                },
                沉浸: false
            }
            mysou = getMyVar("mySou", "");
            ssurl = MY_URL.replace("**", mysou);
            log(ssurl)
            if (mysou != "") {
                setPreResult(d);
                html = 获取源码(ssurl);
                自动搜索('');
            } else {

                if (page == 1) {
                    let sotu = "https://hikerfans.com/more/";
                    let CT = Date.now();
                    let flie = ["电影", "剧集", "动漫", "综艺"];
                    let flei = getItem("rskey", "电影").replace("剧集", "电视剧");
                    d.push({
                        title: '<span style="color:#ff6600"><b>\t热搜榜\t\t\t</b></span>',
                        desc: '<b><span style="color:#19B89D">' + flei + "</span></b>\t\t",
                        url: $(flie, 2, "切换类型").select(() => {
                            setItem("rskey", input);
                            refreshPage(false);
                            return "hiker://empty";
                        }),
                        pic_url: sotu + "60.jpg",
                        col_type: "avatar"
                    }, {
                        col_type: "big_blank_block"
                    }, {
                        col_type: "line"
                    }, {
                        col_type: "big_blank_block"
                    });

                    //let rso = "https://waptv.sogou.com/hotsugg";
                    let LT;
                    try {
                        eval("var res = " + readFile("hiker://files/cache/dzHouse/reso.js"), 0);
                        LT = res.lt;
                    } catch (e) {
                        LT = "0";
                    };

                    if (!fileExist("hiker://files/cache/dzHouse/reso.js") || CT > (LT + 2 * 24 * 60 * 60 * 1000) || getItem("jcbh", "0") != flei) {
                        setItem("jcbh", flei);
                        let url = "https://news.myquark.cn/v2/toplist/movie?&channel=" + flei + "&rank_type=%E6%9C%80%E7%83%AD";
                        //let 小说 = "https://news.myquark.cn/v2/toplist/novel?&channel=男频&rank_type=%E6%9C%80%E7%83%AD";
                        let json = (JSON.parse(fetch(url)).data || []).replace(/title>/g, "f_title>").replace(/src>/g, "f_src>").replace(/area>/g, "f_area>");
                        let lists = pdfa(json, "body&&item")
                            //log(list)
                            let trend = ["-", "↑", "↓"];
                        let list = [];
                        for (let i in lists) {
                            let li = lists[i];
                            let title = pdfh(li, "f_title&&Text");
                            let area = pdfh(li, "f_area&&Text");
                            let category = pdfh(li, "category&&Text");
                            let year = pdfh(li, "year&&Text");
                            let hot = pdfh(li, "hot_score&&Text");
                            let episode = pdfh(li, "episode_count&&Text");
                            let score = pdfh(li, "score_avg&&Text").replace(/^0$/g, "暂无");
                            let hot_trend = pdfh(li, "hot_trend&&Text");
                            let pai = parseInt(i) + 1 + "\t";
                            let tren = trend.at(hot_trend);
                            let hot_tr = (hot / 10000).toFixed(1);
                            let hot_tren = "热搜指数：" + hot_tr + "万";

                            let hot_tre = hot_tr > 65 ? hot_tren.fontcolor("#00cc99") : hot_tr < 33 ? hot_tren : hot_tren.fontcolor("#1e90ff");

                            let one = pai == 1 ? pai.fontcolor("#ff1100") : pai == 2 ? pai.fontcolor("#ff8800") : pai == 3 ? pai.fontcolor("#FFA500") : pai == 4 ? pai.fontcolor("#ffcc66") : pai == 5 ? pai.fontcolor("#f0e68c") : pai.fontcolor("#999999");

                            let tit = "‘‘’’<b>" + one + title + "</b><br>\t\t<small>" + (year + " | " + area + " | " + category).fontcolor("#FA7298") + "</small>";

                            let des = "‘‘’’\t\t" + (episode + "<br>\t\t评分：" + score).fontcolor("#274c5e") + "<br>\t\t" + hot_tre;

                            list.push(
                                tit + "$" + des + "$$" + pdfh(li, "f_src&&Text") + "$$$" + title)
                        };
                        var reso = {
                            "lt": CT,
                            "list": list
                        };
                        saveFile("hiker://files/cache/dzHouse/reso.js", JSON.stringify(reso), 0);
                    };
                    let list = typeof(reso) != "undefined" ? reso.list : res.list;
                    for (let r of list) {
                        d.push({
                            title: r.split("$")[0],
                            desc: r.split("$")[1].split("$$")[0],
                            img: r.split("$$")[1].split("$$$")[0],
                            url: r.split("$$$")[1] + $('#noLoading#').lazyRule(() => {
                                putMyVar("mySou", input.split("#")[0]);
                                refreshPage();
                                return "toast://搜索中";
                            }),
                            col_type: "movie_1_vertical_pic"
                        })
                    };
                    deleteItemByCls("cls_load");
                    setResult(d);
                };
            }

            setResult(d);
        }, MY_PAGE);
        d1 = [];
        设置(d1);
        设置1 = d1[0].url;
        let urlu = [首页, 分类, 追更, 搜索, 设置1];
        let titll = ["首页", "分类", "追更", "搜索", "设置"]
        let 图 = "https://hikerfans.com/more/";
        let pici = ["59.png", "100.png", "102.png", "104.png", "108.png"];
        titll.forEach((title, index) => {
            cates.push({
                title: title,
                url: urlu[index],
                pic_url: 图 + pici[index],
                col_type: 'icon_5'
            })
        });
    }
    return cates;
}
